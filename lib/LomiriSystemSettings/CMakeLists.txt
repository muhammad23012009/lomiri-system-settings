add_library(LomiriSystemSettings SHARED item-base.cpp item-base.h)
set_target_properties(LomiriSystemSettings PROPERTIES
VERSION 1.2.0
SOVERSION 1
)

target_link_libraries(LomiriSystemSettings Qt5::Core Qt5::Gui Qt5::Quick Qt5::Qml)

install(TARGETS LomiriSystemSettings LIBRARY DESTINATION ${LIBDIR})
install(FILES item-base.h ItemBase plugin-interface.h PluginInterface
DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}/LomiriSystemSettings")

set(SYSTEMSETTINGS_LIB LomiriSystemSettings)

join_paths(PC_LIBDIR "\${prefix}" "${LIBDIR}")
join_paths(PC_INCLUDEDIR "\${prefix}" "${CMAKE_INSTALL_INCLUDEDIR}")
join_paths(PC_PLUGIN_MANIFEST_DIR "\${prefix}" "${PLUGIN_MANIFEST_DIR_BASE}")
join_paths(PC_PLUGIN_MODULE_DIR "\${libdir}" "${PLUGIN_MODULE_DIR_BASE}")
join_paths(PC_PLUGIN_PRIVATE_MODULE_DIR "\${libdir}" "${PLUGIN_PRIVATE_MODULE_DIR_BASE}")
join_paths(PC_PLUGIN_QML_DIR "\${prefix}" "${PLUGIN_QML_DIR_BASE}")

configure_file(LomiriSystemSettings.pc.in LomiriSystemSettings.pc @ONLY)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/LomiriSystemSettings.pc DESTINATION ${LIBDIR}/pkgconfig)
