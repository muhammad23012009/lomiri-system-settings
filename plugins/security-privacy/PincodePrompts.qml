import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.ListItems 1.3 as ListItems
import SystemSettings 1.0
import Lomiri.Components.Popups 1.3
import QtQuick.Layouts 1.1
import Lomiri.SystemSettings.SecurityPrivacy 1.0

ItemPage {
    id: page
    objectName: "PinCodePromptsPage"
    title: i18n.tr("Digits passcode")

    flickable: scrollWidget
    header: PageHeader {
        id: pageHeader
        title: i18n.tr("Digits passcode")
        flickable: scrollWidget
        leadingActionBar.actions: [
            Action {
                iconName: "back"
                text: i18n.tr("Back")
                onTriggered: pageStack.removePages(page);
            }
        ]
        trailingActionBar.actions: [
            Action {
                iconName: "edit"
                text: i18n.tr("edit")
                onTriggered: page.passCodeChangeRequested()
            }
        ]
    }

    signal passCodeChangeRequested()

    LomiriSecurityPrivacyPanel {
        id: securityPrivacy
    }

    ListModel {
        id: promptsModel

        function findIndex(pinCodePromptManager) {
            for(var i = 0; i < count; i++) {
                var element = get(i);
                if (pinCodePromptManager === element.manager) {
                    return i;
                }
            }
            return 0;
        }

        Component.onCompleted: {
            append({name: i18n.tr("Keyboard prompt"), description: i18n.tr("Default keyboard input"), manager: "PinPrompt"})
            append({name: i18n.tr("Circle pattern"), description: i18n.tr("Click or swipe on digits to unlock"), manager: "ClockPinPrompt"})
        }
    }

    Flickable {
        id: scrollWidget
        anchors.fill: parent
        contentHeight: contentItem.childrenRect.height
        boundsBehavior: (contentHeight > page.height) ?
                            Flickable.DragAndOvershootBounds :
                            Flickable.StopAtBounds
        /* Set the direction to workaround
           https://bugreports.qt-project.org/browse/QTBUG-31905 otherwise the UI
           might end up in a situation where scrolling doesn't work */
        flickableDirection: Flickable.VerticalFlick

        Column {
            id: content
            anchors.left: parent.left
            anchors.right: parent.right

            SettingsItemTitle {
                text: i18n.tr("Select pin code prompt:")
            }

            ListItems.ItemSelector {
                id: pincodePromptSelector
                expanded: true

                model:  promptsModel
                delegate: OptionSelectorDelegate {
                    height: units.gu(5)
                    text: name
                    subText: description
                }
                onDelegateClicked: {
                    if (index === selectedIndex) {
                        return; // nothing to do
                    }

                    securityPrivacy.pinCodePromptManager = promptsModel.get(index).manager
                }
            }

            Binding {
                target: pincodePromptSelector
                property: "selectedIndex"
                value: promptsModel.findIndex(securityPrivacy.pinCodePromptManager)
            }
        }
    }
}
