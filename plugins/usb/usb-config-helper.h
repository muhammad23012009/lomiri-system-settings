/*
 * Copyright (C) 2024 UBports Foundation
 *
 * Authors:
 *    Muhammad <muhammad23012009@hotmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as published
 * by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef USB_CONFIG_HELPER_H
#define USB_CONFIG_HELPER_H

#include <QObject>
#include <QDBusInterface>
#include <QDBusConnection>
#include <QDBusReply>
#include <QDebug>

#define USB_MODED_SERVICE "com.meego.usb_moded"
#define USB_MODED_PATH "/com/meego/usb_moded"

class UsbConfigHelper : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool developerModeCapable READ developerModeCapable CONSTANT)
    Q_PROPERTY(ConfigMode mode READ getMode WRITE setMode NOTIFY modeChanged)
    Q_PROPERTY(bool adbEnabled READ getAdbEnabled WRITE setAdbEnabled NOTIFY adbEnabledChanged)

public:
    enum ConfigMode {
        USB_INVALID = -1,
        USB_CHARGING_ONLY = 0,
        USB_MTP,
        USB_RNDIS,
    };
    Q_ENUM(ConfigMode)

    UsbConfigHelper(QObject *parent = nullptr);
    ~UsbConfigHelper() {};

    ConfigMode str_to_mode(QString str);
    QString mode_to_str(ConfigMode mode);

    bool developerModeCapable();

    ConfigMode getMode();
    void setMode(ConfigMode mode);

    bool getAdbEnabled();
    void setAdbEnabled(bool enabled);

public Q_SLOTS:
    void targetModeChanged(QString mode);

Q_SIGNALS:
    void modeChanged();
    void adbEnabledChanged();

private:
    void setMode(QString str);
    void updateValues(QString mode);

    ConfigMode m_mode = USB_INVALID;
    bool m_adbEnabled = false;
    QDBusInterface *m_iface;
};

#endif // USB_CONFIG_HELPER_H
