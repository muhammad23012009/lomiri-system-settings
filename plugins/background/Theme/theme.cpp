/*
 * Copyright (C) 2023 UBports Foundation
 * 
 * Authors:
 *    Muhammad <muhammad23012009@hotmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as published
 * by the Free Software Foundation.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "theme.h"

Theme::Theme(QObject *parent) :
    QObject(parent),
    m_settings((QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + QString("/lomiri-ui-toolkit/theme.ini")), QSettings::IniFormat)
{
}
Theme::~Theme()
{
}

bool Theme::darkModeEnabled()
{
     return m_settings.value("theme").toString() == DARK_THEME;
}

void Theme::setDarkModeEnabled(bool enabled)
{
    enabled ? enableDarkMode() : enableLightMode();
    Q_EMIT darkModeChanged();
}

void Theme::enableLightMode()
{
    m_settings.setValue("theme", LIGHT_THEME);
}

void Theme::enableDarkMode()
{
    m_settings.setValue("theme", DARK_THEME);
}
